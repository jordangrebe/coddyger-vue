/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev Associates
| Website :: https://www.ultrondev.com/
|
*/
import jQ from 'jquery'
import toastr from 'toastr'
// import zxcvbn from 'zxcvbn'

let coddyger = {
  form_validator: {
    is_empty: function (value) {
      return (value.val() === undefined || value.val() === null || value.length <= 0 || value.val() === '')
    },
    is_email: function (value) {
      // eslint-disable-next-line no-useless-escape
      return /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(value.val())
    },
    is_url: function (value) {
      return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value.val())
    },
    is_number: function (value) {
      return /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value.val())
    },

    typing_empty_validator: function (element) {
      element.on('input', function () {
        (coddyger.form_validator.is_empty(jQ(this)) === true ? jQ(this).addClass('is-invalid').css('border', '1px solid red') : jQ(this).removeClass('is-invalid').addClass('is-valid').css('border', '1px solid green'))
      })
    },
    typing_email_validator: function (element) {
      element.on('input', function () {
        (coddyger.form_validator.is_email(jQ(this)) === false || coddyger.form_validator.is_empty(jQ(this)) === true ? jQ(this).addClass('is-invalid').css('border', '1px solid red') : jQ(this).removeClass('is-invalid').addClass('is-valid').css('border', '1px solid green'))
      })
    },
    typing_number_validator: function (element) {
      element.on('input', function () {
        (coddyger.form_validator.is_number(jQ(this)) === false || coddyger.form_validator.is_empty(jQ(this)) === true ? jQ(this).addClass('is-invalid').css('border', '1px solid red').attr('data-toggle', 'kt-tooltip').attr('title', 'Must be integer') : jQ(this).removeClass('is-invalid').addClass('is-valid').css('border', '1px solid green').attr('data-toggle', '').attr('title', ''))
      })
    },
    typing_url_validator: function (element) {
      element.on('input', function () {
        (coddyger.form_validator.is_url(jQ(this)) === false || coddyger.form_validator.is_empty(jQ(this)) === true ? jQ(this).addClass('is-invalid').css('border', '1px solid red') : jQ(this).removeClass('is-invalid').addClass('is-valid').css('border', '1px solid green'))
      })
    },
    typing_password_compare_validator: function (password, passwordConf, message) {
      passwordConf.on('input', function () {
        let inputAlert = jQ(this).parent().find('.input_alert')
        if (jQ(this).val() !== password.val() || coddyger.form_validator.is_empty(jQ(this)) === true) {
          inputAlert.html(message).addClass('text-danger')
          jQ(this).addClass('is-invalid').css('border', '1px solid red')
        } else {
          inputAlert.html('')
          jQ(this).removeClass('is-invalid').addClass('is-valid').css('border', '1px solid green')
        }
      })
    }
  },
  string: {
    is_empty: function (value) {
      return (value === undefined || value === null || value.length <= 0 || value === '')
    },
    is_email: function (value) {
      // eslint-disable-next-line no-useless-escape
      return /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(value)
    },
    is_url: function (value) {
      return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value)
    },
    is_number: function (value) {
      return /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value)
    }
  },
  initComingSoon: () => {
    jQ('.comingsoon[href="#"]').on('click', function () {
      toastr.options = {
        'progressBar': true,
        'positionClass': 'toast-top-right',
        'preventDuplicates': true,
        'extendedTimeOut': 2000
      }
      toastr.info('Cette fonctionnalité est en conception ou en cours de maintenance.', 'Fonctionnalité indisponible.')
      return false
    })
  },
  initSystemSignature: () => {
    console.log('/*\n' +
      '|--------------------------------------------------------------------------\n' +
      '| Coddyger - PHP Environement\n' +
      '|--------------------------------------------------------------------------\n' +
      '|\n' +
      '| Authors :: UltronDev Associates\n' +
      '| Website :: https://www.ultrondev.com/\n' +
      '|\n' +
      '*/\n')
  },
  konsole: (string, type = 'log') => {
    return (type === 'error' ? console.error('thanos_console::' + string) : console.log('thanos_console::' + string))
  },
  doReload: function (reloadFunction, reloadTime) {
    setInterval(reloadFunction, reloadTime)
  },
  doSessionSentinel: function (gotoUri) {
    jQ.get(coddyger.app.appSources() + 'cdg_ajax.php?method=session_sentinel', function (res) {
      let isRight = res.isRight

      if (isRight === 'no') {
        toastr.error('Session dupliquée détectée')
        setTimeout(function () {
          location.href = '/auth/bad-session?goto=' + gotoUri
        }, 2000)
      } else if (isRight === 'expired') {
        toastr.error('Cette session a expiré')
        setTimeout(function () {
          location.href = '/auth/expired-session?goto=' + gotoUri
        }, 2000)
      } else {
        console.log('session_stat :: ' + isRight)
      }
    })
  },
  countDown: function (initDate, initSelector) {
    let countDownDate = new Date(initDate).getTime()
    let x = setInterval(function () {
      let now = new Date().getTime()
      let distance = countDownDate - now
      let days = Math.floor(distance / (1000 * 60 * 60 * 24))
      let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
      let seconds = Math.floor((distance % (1000 * 60)) / 1000)

      jQ(initSelector).html('' + days + ' j' + hours + ':' + minutes + ':' + seconds + '')

      if (distance < 0) {
        clearInterval(x)
        jQ(initSelector).html('Expiré')
      }
    }, 1000)
  },
  copyText: function (element) {
    let range, selection

    if (document.body.createTextRange) {
      range = document.body.createTextRange()
      range.moveToElementText(element)
      range.select()
    } else if (window.getSelection) {
      selection = window.getSelection()
      range = document.createRange()
      range.selectNodeContents(element)
      selection.removeAllRanges()
      selection.addRange(range)
    }
    try {
      document.execCommand('copy')
      toastr.info('copié dans le papier-press')
      console.log('thanos::copié dans le papier-press')
    } catch (err) {
      toastr.info('unable to copy text')
      console.log('thanos::unable to copy text')
    }
  },
  detectSingleView: function (string) {
    let c, d, g, data
    c = string.charAt(0)
    d = string.charAt(1)
    g = string.charAt(2)

    data = c + d + g
    return data === 'cdg'
  },
  noDataText: function (content, text, classes = 'white') {
    let container = jQ('<div>').html(text).addClass(classes)
    content.html(container)
  },
  setCookie: function (cookieName, cookieValue, cookieExpire) {
    if (cookieName !== '' & cookieValue !== '' && cookieExpire !== '') {
      let today = new Date()
      let expires = new Date()
      expires.setTime(today.getTime() + (cookieExpire * 24 * 60 * 60 * 1000))
      document.cookie = cookieName + '=' + encodeURIComponent(cookieValue) + ';expires=' + expires.toGMTString()
    } else {
      console.error('set_cookie: all parameters are required.')
    }
  },
  getCookie: function (cookieName) {
    if (cookieName !== '') {
      let name = cookieName + '='
      let ca = document.cookie.split(';')
      for (let i = 0; i < ca.length; i++) {
        let c = ca[i]
        while (c.charAt(0) === ' ') c = c.substring(1)
        if (c.indexOf(name) !== -1) return c.substring(name.length, c.length)
      }
    } else {
      console.error('get_cookie: cookie_name is required.')
    }
  },
  tabState: function (el) {
    el.find('a').click(function (e) {
      e.preventDefault()
      jQ(this).tab('show')
    })

    // store the currently selected tab in the hash value
    jQ('ul.nav-tabs > li > a').on('shown.bs.tab', function (e) {
      window.location.hash = jQ(e.target).attr('href').substr(1)
    })

    // on load of the page: switch to the currently selected tab
    let hash = window.location.hash
    el.find('a[href="' + hash + '"]').tab('show')
  },
  getDir: function () {
    let curUrl = window.document.location
    let res = curUrl.pathname.split('/')
    let data = res.slice(0)[1]

    return (data) || ''
  },
  getSubdir: function () {
    let curUrl = window.document.location
    let res = curUrl.pathname.split('/')
    let data = res.slice(0)[2]

    return (data) || ''
  },
  getRef: function () {
    let curUrl = window.document.location
    let res = curUrl.pathname.split('/')
    let data = res.slice(0)[3]

    return (data) || ''
  },
  getYear: function () {
    let d = new Date()
    return d.getFullYear()
  },
  ucFirst: function (string) {
    return string[0].toUpperCase() + string.slice(1)
  },
  inArray: function (needle, haystack) {
    let length = haystack.length
    for (let i = 0; i < length; i++) {
      if (haystack[i] === needle) return true
    }
    return false
  },
  bufferToImg: function (buffer) {
    let TYPED_ARRAY = new Uint8Array(buffer)
    const STRING_CHAR = String.fromCharCode.apply(null, TYPED_ARRAY)
    let base64String = btoa(STRING_CHAR)

    return 'data:image/jpg;base64, ' + base64String
  },
  currentDate: function () {
    let today = new Date().toLocaleString('fr-FR')

    return today
  },
  doTag: function (tagName, tagAttrs) {
    let tag = document.createElement(tagName)
    if (tagAttrs !== '') {
      for (let x in tagAttrs) {
        let tagAttrName = tagAttrs[x]['name']
        let tagAttrValue = tagAttrs[x]['value']

        tag.setAttribute(tagAttrName, tagAttrValue)
      }
    }
    document.head.appendChild(tag)
  },
  externalJS: function (uriLink, position) {
    let script = document.createElement('script')
    script.setAttribute('src', uriLink)
    script.setAttribute('type', 'javascript/application')

    if (position === 'head') {
      document.head.appendChild(script)
    } else {
      document.body.appendChild(script)
    }
  },
  setPageTitle: function (title) {
    document.title = coddyger.ucFirst(title)
  },
  setPageOg: function (ogArray) {
    let url = ogArray.url
    let type = ogArray.type
    let title = ogArray.title
    let desc = ogArray.description
    let img = ogArray.image

    coddyger.doTag('meta', {
      0: {
        'name': 'property',
        'value': 'og:url'
      },
      1: {
        'name': 'content',
        'value': url
      }
    })
    coddyger.doTag('meta', {
      0: {
        'name': 'property',
        'value': 'og:type'
      },
      1: {
        'name': 'content',
        'value': type
      }
    })
    coddyger.doTag('meta', {
      0: {
        'name': 'property',
        'value': 'og:title'
      },
      1: {
        'name': 'content',
        'value': title
      }
    })
    coddyger.doTag('meta', {
      0: {
        'name': 'property',
        'value': 'og:description'
      },
      1: {
        'name': 'content',
        'value': desc
      }
    })
    coddyger.doTag('meta', {
      0: {
        'name': 'property',
        'value': 'og:image'
      },
      1: {
        'name': 'content',
        'value': img
      }
    })
  }
}

export default coddyger
