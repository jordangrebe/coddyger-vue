import jQ from 'jquery'
import jsonMenu from '../assets/jsonMenu'
import coddyger from './coddyger'

export default class Menu {
  static runMenu () {
    let _menu = new Menu()

    _menu.loadMenu('menu_modules')
    _menu.loadMenu('menu_app_0')
    _menu.loadMenu('menu_app_1')
    _menu.loadMenu('menu_app_2')
  }
  loadMenu (display) {
    let dir = '/' + coddyger.getDir()
    let subdir = '/' + coddyger.getSubdir()

    let menu = null
    let coddygerMenu = jsonMenu['coddyger_menu']
    let status = jsonMenu['status']

    let template = ''
    let active = ''
    let subactive = ''
    let x

    if (display === 'menu_modules') {
      menu = coddygerMenu['menu_modules']
    } else if (display === 'menu_app_0') {
      menu = coddygerMenu['menu_app_0']
    } else if (display === 'menu_app_1') {
      menu = coddygerMenu['menu_app_1']
    } else if (display === 'menu_app_2') {
      menu = coddygerMenu['menu_app_2']
    } else if (display === 'menu_reports') {
      menu = coddygerMenu['menu_reports']
    } else if (display === 'menu_new') {
      menu = coddygerMenu['menu_new']
    }

    if (status === 'ok') {
      let classMenu = jQ('#' + display)
      classMenu.html('')

      for (x in menu) {
        let menuLink = menu[x]['menu_link']
        let menuTitle = menu[x]['menu_title']
        let menuIcon = menu[x]['menu_icon']
        let menuSub = menu[x]['menu_sub']

        if (menuSub !== null) { // Items with submenu
          (menuLink === dir ? active = 'kt-menu__item--open kt-menu__item--here kt-menu__item--active ' : active = '')// Set current menu block as selected and openned

          template += '<li id="' + menuLink + '" class="kt-menu__item kt-menu__item--submenu ' + active + ' " aria-haspopup="true" data-ktmenu-submenu-toggle="hover" data-ktmenu-submenu-mode="accordion">' + ''
          template += '<a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon ' + menuIcon + '"></i><span class="kt-menu__link-text">' + menuTitle + '</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>' + ''
          template += '<div class="kt-menu__submenu">'
          template += '<span class="kt-menu__arrow"></span>'
          template += '<ul class="kt-menu__subnav">'
          for (let y in menuSub) {
            let menuSubTitle = menuSub[y]['menu_sub_title']
            let menuSubLink = menuSub[y]['menu_sub_link']
            let menuSubIcon = menuSub[y]['menu_sub_icon']
            let tmpSubmenuLink = menuSubLink.replace(dir, ''); // Get subdir only from dir

            (tmpSubmenuLink === subdir ? subactive = 'kt-menu__item--active ' : subactive = '') // Set current subdir as active

            template += '<li class="kt-menu__item ' + subactive + '" aria-haspopup="true">' +
              '<a  href="' + menuSubLink + '" class="kt-menu__link">' +
              '<span class="kt-menu__link-text">' +
              '<i class="kt-menu__link-icon ' + menuSubIcon + '"></i>' +
              menuSubTitle +
              '</span>' +
              '</a>' +
              '</li>'
          }
          template += '</ul>'
          template += '</div>'
          template += '</li>'
        } else { // Item without submenu
          (menuLink === dir ? active = 'kt-menu__item--active kt-menu__item--open' : active = '')// Set current dir as active

          template += '' +
            '<li class="kt-menu__item ' + active + '" aria-haspopup="true">' +
            '     <a href="' + menuLink + '" class="kt-menu__link ">' +
            '          <i class="kt-menu__link-icon ' + menuIcon + '"></i>' +
            '          <span class="kt-menu__link-text">' + menuTitle + '</span>' +
            '     </a>' +
            '</li>'
        }
      }
      classMenu.append(template)
    } else {
      console.log('coddygerMenu::unable to fetch data from file. :: ')
    }
  }
}
