import packages from '../../../package'
const config = {
  appName: packages.name,
  serviceName: 'Direction Régionale des Transports',
  cityName: 'Gagnoa',
  stateName: 'Région du Goh',
  author: 'Jordan Grébé'
}
export default config
