import info from '../database/info'
import mysql from 'mysql'

export default class Database {
  static create (name) {
    return name
  }
  static connect () {
    return mysql.createConnection({
      host: info.host,
      user: info.user,
      password: info.password,
      database: info.database
    })
  }
  static query (query, callback) {
    let con = Database.connect()

    con.connect(function (err) {
      if (err) {
        alert(err.sqlMessage)
      } else {
        con.query(query, function (err, result) {
          let obj
          if (err) {
            obj = {error: 'error', resp: null}
            return callback(obj)
          } else {
            obj = {error: null, resp: result}
            return callback(obj)
          }
        })
      }
    })
  }
  static exist (table, column, value, callback) {
    Database.query('SELECT * FROM ' + table + ' WHERE ' + column + ' = "' + value + '"', (Q) => {
      if (Q.resp === null) {
        return 'mysql_error'
      } else {
        let rowCount = Q.resp.length

        if (rowCount >= 1) {
          // eslint-disable-next-line standard/no-callback-literal
          return callback(true)
        } else {
          // eslint-disable-next-line standard/no-callback-literal
          return callback(false)
        }
      }
    })
  }
}
