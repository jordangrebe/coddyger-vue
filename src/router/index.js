import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: require('../templates/frontend/frontend-layout').default,
    children: [
      {
        path: '/',
        name: 'home',
        component: require('../components/frontend/home/home.vue').default
      },
      {
        path: '/questions',
        name: 'questions',
        component: require('../components/frontend/questions/questions.vue').default
      }
    ]
  },
  {
    path: '*',
    redirect: '/'
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
