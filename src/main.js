import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import headMixin from './scripts/head'
// import VueSession from 'vue-session'
window.$ = require('jquery')
window.JQuery = require('jquery')

Vue.mixin(headMixin)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
